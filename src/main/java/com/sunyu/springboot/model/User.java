package com.sunyu.springboot.model;

import java.io.Serializable;


/**
 * Table:
 * @author yu
 * @date 2017-07-27 16:58:53
 *
 */
public class User implements Serializable {

    private static final long serialVersionUID = -6728770412413829339L;

   	private Integer id;
	private String name;
	private String password;
	private String role;
	private String salt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
}