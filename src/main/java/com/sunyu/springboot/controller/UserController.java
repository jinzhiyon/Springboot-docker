package com.sunyu.springboot.controller;


import com.sunyu.springboot.model.User;
import com.sunyu.springboot.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 *
 * @author yu
 * @date 2017-07-27 16:58:53
 *
 */
@Controller
public class UserController {
    /**
     * 日志
     */
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

	@Resource
	private UserService userService;

	@ResponseBody
	@GetMapping(value = "/query")
	public User queryById() {
		return userService.queryById(0);
	}

	@ResponseBody
	@GetMapping(value = "/")
	public String index(){
		return "hello world";
	}


}