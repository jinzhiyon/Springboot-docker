package com.sunyu.springboot.service.impl;

import com.sunyu.springboot.model.User;
import com.sunyu.springboot.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author yu
 * @date 2017-07-27 16:58:53
 *
 */
@Service("userService")
public class UserServiceImpl  implements UserService{

    /**
     * 日志
     */
    private static Logger logger = LoggerFactory.getLogger(UserService.class);

	@Override
	public User queryById(int id) {
        User user = new User();
        user.setName("sunyu");
        user.setPassword("sunyu");
        return user;
	}



}