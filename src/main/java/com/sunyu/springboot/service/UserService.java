package com.sunyu.springboot.service;

import com.sunyu.springboot.model.User;

/**
 *
 * @author yu
 * @date 2017-07-27 16:58:53
 *
 */

public interface UserService {
	/**
	 * 根据id查询数据
	 * @param id
	 * @return
     */
	User queryById(int id);
}